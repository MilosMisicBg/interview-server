package com.demo.interview;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EntityScan(basePackages = {"com.demo.interview.model"})
@ComponentScan(basePackages = {"com.demo.interview"})
public class InterviewAppApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(InterviewAppApplication.class, args);
    }

}
