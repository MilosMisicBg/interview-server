package com.demo.interview.services;

import com.demo.interview.model.Farm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FarmService {

    @Autowired
    private FarmRepository farmRepository;

    public List<Farm> getAllFarms() {
        return (List<Farm>) farmRepository.findAll();
    }

    public Farm getFarm(int id) {
        return farmRepository.findById(id).orElse(null);
    }

    public void addFarm(Farm farm) {
        farmRepository.save(farm);
    }

    public void addFarms(List<Farm> farms) {
        farmRepository.saveAll(farms);
    }

    public void deleteFarm(int id) {
        farmRepository.deleteById(id);
    }

}

interface FarmRepository extends CrudRepository<Farm, Integer> {
}