package com.demo.interview.services;

import com.demo.interview.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    public List<Customer> getAllCustomers() {
        return (List<Customer>) customerRepository.findAll();
    }

    public Customer getCustomer(int id) {
        return customerRepository.findById(id).orElse(null);
    }

    public void addCustomer(Customer customer) {
        customerRepository.save(customer);
    }

    public void addCustomers(List<Customer> customers) {
        customerRepository.saveAll(customers);
    }

    public void deleteCustomer(int id) {
        customerRepository.deleteById(id);
    }

}

interface CustomerRepository extends CrudRepository<Customer, Integer> {
}