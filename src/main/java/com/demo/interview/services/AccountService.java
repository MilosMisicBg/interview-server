package com.demo.interview.services;

import com.demo.interview.model.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountService {

    @Autowired
    private AccountRepository accountRepository;

    public List<Account> getAllAccounts() {
        return (List<Account>) accountRepository.findAll();
    }

    public Account getAccount(int id) {
        return accountRepository.findById(id).orElse(null);
    }

    public void addAccount(Account account) {
        accountRepository.save(account);
    }

    public List<Account> addAccounts(List<Account> accounts) {
        return (List<Account>) accountRepository.saveAll(accounts);
    }

    public void deleteAccount(int id) {
        accountRepository.deleteById(id);
    }

}

interface AccountRepository extends CrudRepository<Account, Integer> {
}
