package com.demo.interview.services;

import com.demo.interview.model.Account;
import com.demo.interview.model.Farm;
import com.demo.interview.model.User;
import com.demo.interview.security.SecureHash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public List<User> getAllUsers() {
        return (List<User>) userRepository.findAll();
    }


    public User getUser(int id) {
        return userRepository.findById(id).orElse(null);
    }

    public void addUser(User user) {
        userRepository.save(user);
    }

    public void addUsers(List<User> users) {
        userRepository.saveAll(users);
    }

    public void deleteUser(int id) {
        userRepository.deleteById(id);
    }

    public User getUserByUserName(String username) {
        return userRepository.findByUsername(username);
    }

    public List<Account> getAccountsByUsername(String username) {
        User user = getUserByUserName(username);
        if (user != null) {
            return user.getAccounts();
        }
        return Collections.emptyList();
    }

    public List<Farm> getFarmsByUsername(String username) {
        User user = getUserByUserName(username);
        if (user != null && user.getAccounts() != null) {
            List<Farm> farms = new ArrayList<>();
            user.getAccounts().forEach(account -> farms.addAll(account.getFarms()));
            return farms;
        }
        return Collections.emptyList();
    }

    public boolean validateUser(User clientUser) {
        if (clientUser != null && clientUser.getUsername() != null) {
            User serverUser = getUserByUserName(clientUser.getUsername());
            if (serverUser != null) {
                clientUser.setPassword(SecureHash.getSecureHash(clientUser.getPassword(), serverUser.getSalt()));
                return clientUser.getPassword().equals(serverUser.getPassword());
            }
        }
        return false;
    }
}

interface UserRepository extends CrudRepository<User, Integer> {
    User findByUsername(String username);
}