package com.demo.interview.controllers;

import com.demo.interview.model.Account;
import com.demo.interview.model.Farm;
import com.demo.interview.model.User;
import com.demo.interview.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping("")
    public List<User> getAllUsers() {
        return userService.getAllUsers();
    }

    @RequestMapping("/accounts/{username}")
    public List<Account> getAccountsByUser(@PathVariable String username) {
        return userService.getAccountsByUsername(username);
    }

    @RequestMapping("/farms/{username}")
    public List<Farm> getFarmsByUser(@PathVariable String username) {
        return userService.getFarmsByUsername(username);
    }

    @RequestMapping("/validate/{username}/{password}")
    public String validateUser(@PathVariable String username, @PathVariable String password) {
        return String.valueOf(userService.validateUser(new User(username, password)));
    }

    @RequestMapping(value = "/validate", method = RequestMethod.POST)
    public String validateUserWithPost(@RequestBody User user) {
        return String.valueOf(userService.validateUser(user));
    }

}
