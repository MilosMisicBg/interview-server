package com.demo.interview.controllers;

import com.demo.interview.model.Farm;
import com.demo.interview.services.FarmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/farms")
public class FarmController {

    @Autowired
    private FarmService farmService;

    @RequestMapping("")
    public List<Farm> getAllFarms() {
        return farmService.getAllFarms();
    }
}
