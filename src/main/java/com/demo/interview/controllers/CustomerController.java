package com.demo.interview.controllers;

import com.demo.interview.model.Customer;
import com.demo.interview.model.User;
import com.demo.interview.services.CustomerService;
import com.demo.interview.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/customers")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @RequestMapping("")
    public List<Customer> getAllCustomers() {
        return customerService.getAllCustomers();
    }
}
