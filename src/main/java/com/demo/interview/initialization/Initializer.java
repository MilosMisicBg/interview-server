package com.demo.interview.initialization;

import com.demo.interview.model.Account;
import com.demo.interview.model.Customer;
import com.demo.interview.model.Farm;
import com.demo.interview.model.User;
import com.demo.interview.security.SecureHash;
import com.demo.interview.services.AccountService;
import com.demo.interview.services.CustomerService;
import com.demo.interview.services.FarmService;
import com.demo.interview.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Component
public class Initializer implements CommandLineRunner {

    @Autowired
    private UserService userService;
    @Autowired
    private AccountService accountService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private FarmService farmService;

    @Override
    public void run(String... args) {
        initializeDatabase();
    }

    private void initializeDatabase() {

        Farm farm1 = new Farm("Farm 1");
        Farm farm2 = new Farm("Farm 2");
        Farm farm3 = new Farm("Farm 3");
        Farm farm4 = new Farm("Farm 4");
        Farm farm5 = new Farm("Farm 5");
        Farm farm6 = new Farm("Farm 6");
        Farm farm7 = new Farm("Farm 7");
        Farm farm8 = new Farm("Farm 8");

        List<Farm> farms = Arrays.asList(farm1, farm2, farm3, farm4, farm5, farm6, farm7, farm8);

        farmService.addFarms(farms);

        Account account1 = new Account("Account 1");
        Account account2 = new Account("Account 2");
        Account account3 = new Account("Account 3");
        Account account4 = new Account("Account 4");

        account1.setFarms(Arrays.asList(farm1, farm2));
        account2.setFarms(Arrays.asList(farm3, farm4));
        account3.setFarms(Arrays.asList(farm5, farm6));
        account4.setFarms(Arrays.asList(farm7, farm8));

        List<Account> accounts = Arrays.asList(account1, account2, account3, account4);

        accountService.addAccounts(accounts);

        User user1 = new User("user1", "123321", SecureHash.getSalt());
        User user2 = new User("user2", "abc123", SecureHash.getSalt());
        User user3 = new User("user3", "123abc", SecureHash.getSalt());

        user1.setAccounts(Collections.singletonList(account1));
        user2.setAccounts(Arrays.asList(account2, account3));
        user3.setAccounts(Collections.singletonList(account4));

        List<User> users = Arrays.asList(user1, user2, user3);

        userService.addUsers(users);

        Customer customer1 = new Customer("Customer 1");
        Customer customer2 = new Customer("Customer 1");
        Customer customer3 = new Customer("Customer 1");
        Customer customer4 = new Customer("Customer 1");

        customer1.setAccount(account1);
        customer2.setAccount(account2);
        customer3.setAccount(account3);
        customer4.setAccount(account4);

        List<Customer> customers = Arrays.asList(customer1, customer2, customer3, customer4);

        customerService.addCustomers(customers);
    }

}
